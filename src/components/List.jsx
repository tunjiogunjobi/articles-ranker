import React from "react";
import "../App.css";



export const List = (props) => {
    return (
        <div>
            <ul>
                {props.model.items.map((item, index) => <li id={index}>{item}</li>)}
            </ul>
        </div>
    );
};