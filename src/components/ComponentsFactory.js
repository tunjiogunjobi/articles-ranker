import React from "react";

export const ComponentFactory = (props) => {
  const { article } = props;
  return (
    <div>
      {
        article.map.map((item, index) => {
          let Component = item.type;
          return (
            <Component key={index} {...article.model} />
          )
        })
      }
    </div>
  )
};