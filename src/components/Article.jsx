import React from "react";
import "../App.css";
import { Heading } from "./Heading"
import { Paragraph } from "./Paragraph"
import { ArticleImage } from "./ArticleImage";
import { List } from "./List";




export const Article = (props) => {

  //Todo dynamically select which component is rendered and remove switch statement.

  return (
    <div>
      {

        props.article.body.map((item, index) => {
          switch (item.type) {
            case "heading":
              return <Heading key={index} model={item.model} />
            case "paragraph":
              return <Paragraph key={index} model={item.model} />

            case "image":
              return <ArticleImage key={index} model={item.model} />
            case "list":
              return <List key={index} model={item.model} />

            default:
              return null;
          }
        })}
    </div>

  );
};