import React from "react";
import "../App.css";


// Todo Lazy loading of image
export const ArticleImage = (props) => {
    return (
        <div>
            <img src={props.model.url}
                alt={props.model.altText}
                height={props.model.height}
                width={props.model.height}
            />
        </div>
    );
};