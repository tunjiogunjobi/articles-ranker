//import logo from './logo.svg';
import React, { useEffect, useState } from 'react';

import axios from 'axios'

import './App.css';
import { Article } from './components/Article';
import { Ranking } from './components/Ranking';


function App() {

  const [isLoading, setLoading] = useState(true);
  const [article, setArticle] = useState();
  const [counter, setCounter] = useState(1);
  const [showRanking, setShowRanking] = useState(false);

  const url = 'http://localhost:3001/';




  useEffect(() => {
    const fetchData = async () => {

      // async wait is not supported in internet explorer and older browsers
      try {
        const response = await axios.get(url.concat("article-".concat(counter)))
        const article = response.data
        console.log(article)
        setArticle(article);
        setLoading(false);

        document.title = article.title


      } catch (error) {
        console.error(error);
        console.log(error.response.request.status)
        if (error.response.request.status === 404) {
          setShowRanking(true)
          console.log(showRanking)
        }

      }

    };
    fetchData();
  }, [counter, showRanking]);




  if (isLoading) {
    return <div className="App">Loading...</div>;
  }



  const handleClick = event => {
    setCounter(counter + 1);


    console.log("Counter is", counter)
    console.log("Button clicked")

  }


  return (
    <>

      {showRanking ? <Ranking /> : <div className="App">
        <h1>Articles Ranker</h1>
        <Article article={article} />
        <button onClick={handleClick}>Next Article</button>
      </div>}

    </>
  );
}

export default App;
