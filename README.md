# BBC News React Coding Test - Articles Ranker

A news site that renders random articles, and has the ability for the user to rank the articles once they have read them all.

Usage:  npm start, npm run stubapi

To Do:

1. Show title of read articles on the ranking page with a five stars below. Reader can then rank the article. Save ranking to the database using a POST.
2. Implement some test.
3. Apply some CSS styling
4. URL to show which article is being read.
5. Deal with server error.
6. Prefetch next article.

